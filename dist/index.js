const RainbowSDK = require("rainbow-node-sdk");
const express = require("express");
const bodyParser = require("body-parser");
const cson = require("cson");
const fs = require("fs");
const path = require("path");
const crypto = require("crypto");
const process = require("process");
const logger = require("./modules/logger")

const app = express();
const config = cson.load(process.env.RAINBOW_GATEWAY_CONFIG_PATH || "config.cson");
let rainbow = new RainbowSDK(config);

const PORT = process.env.PORT || config.webapi.port || 8000;

const sendMessage = function(loginEmail, message) {
    return rainbow.contacts.getContactByLoginEmail(loginEmail).then( (contact) => {
        return rainbow.im.sendMessageToContact(message, contact);
    });
};

const sendMessageBubble = function(jid, message) {
    return rainbow.im.sendMessageToBubbleJid(message, jid);
};

const sendAttachment = function(loginEmail, attachment) {
    return rainbow.fileStorage.getUserQuotaConsumption().then( (consumption) => {

        // Get some space before sending new file
        if (consumption.currentValue + attachment.fileSize > consumption.maxValue) {
            logger.text.info("Max consumption is reached, remove half of the files");
            let fdArray = rainbow.fileStorage.getAllFilesSent();
            for (var i = 0; i < (fdArray.length / 2); i++) {
                rainbow.fileStorage.removeFile(fdArray[i]);
            }
            logger.text.info("File deletion ended successfully");
        }

        // Send the file
        return rainbow.contacts.getContactByLoginEmail(loginEmail).then( (contact) => {
            return rainbow.conversations.openConversationForContact(contact).then( (conversation) => {
                if (attachment.message === undefined || attachment.message === "")  {
                    attachment.message = attachment.filename;
                    logger.text.debug("Using file name if no message found");
                }
                return rainbow.fileStorage.uploadFileToConversation(conversation, {
                    "path": attachment.path,
                    "name": attachment.filename,
                    "type": attachment.contentType,
                    "size": attachment.fileSize
                }, attachment.message)
            });
        });
    });
};

const sendAttachmentBubble = function(jid, attachment) {
    return rainbow.fileStorage.getUserQuotaConsumption().then( (consumption) => {

        // Get some space before sending new file
        if (consumption.currentValue + attachment.fileSize > consumption.maxValue) {
            logger.text.info("Max consumption is reached, remove half of the files");
            let fdArray = rainbow.fileStorage.getAllFilesSent();
            for (var i = 0; i < (fdArray.length / 2); i++) {
                rainbow.fileStorage.removeFile(fdArray[i]);
            }
            logger.text.info("File deletion ended successfully");
        }

        // Send the file
        return rainbow.bubbles.getBubbleByJid(jid).then( (bubble) => {
            return rainbow.fileStorage.uploadFileToBubble(bubble, {
                        "path": attachment.path,
                        "name": attachment.filename,
                        "type": attachment.contentType,
                        "size": attachment.fileSize
                    }, attachment.message)
        })
    });
};

rainbow.start().then( (result) => {
    logger.text.info("Rainbow SDK Ready");

    rainbow.events.on("rainbow_onmessagereceived", (message) => {
        logger.text.info("Received a message : ", message);
        if (message.hasOwnProperty("fromBubbleJid") && message.fromBubbleJid !== null) {
            logger.text.info("Received message from bubble: ", message.fromBubbleJid);
            logger.text.info("Message: ", message.content);
        } else if (message.fromBubbleJid === null) {
            logger.text.info("Received message from userLogin: ", message.conversation.contact.loginEmail);
            logger.text.info("Message: ", message.content);
        }
    });

    // Use some middlewares for body parsing
    app.use(bodyParser.json({limit: config.webapi.bodySizeLimit, extended: true}));
    app.use(bodyParser.urlencoded({limit: config.webapi.URLSizeLimit, extended: true}));


    // Register sendMessage endpoint
    app.post("/message", (request, response) => {
        logger.text.info("Trying to send a message");

        if (request.body.hasOwnProperty("userLogin")) {
            let loginEmail = request.body.userLogin;
            let message = request.body.hasOwnProperty("message") ? request.body.message : " ";

            sendMessage(loginEmail, message).then(() => {
                logger.text.info("Send message successfully to userLogin");
                response.json({
                    "success": true
                });
            }).catch((error) => {
                logger.text.error("Failed to send message to userLogin, got error : ", error);
                response.status(500);
                response.json({
                    "success": false,
                    "error": error
                });
            });
        } else if (request.body.hasOwnProperty("bubble")) {
            let jid = request.body.bubble;
            let message = request.body.hasOwnProperty("message") ? request.body.message : " ";

            sendMessageBubble(jid, message).then(() => {
                logger.text.info("Send message successfully to bubble");
                response.json({
                    "success": true
                });
            }).catch((error) => {
                logger.text.error("Failed to send message to bubble, got error : ", error);
                response.status(500);
                response.json({
                    "success": false,
                    "error": error
                });
            });
        } else {
            logger.text.error("Failed to send message, you must provide either a user email or a jid");
            response.status(400);
            response.json({
                "success": false,
                "error": "Error: You must provide either a user email or a jid"
            });
        }
    });

    app.post("/attachment", (request, response) => {
        logger.text.info("Trying to send a message with attachment");

        let message = request.body.hasOwnProperty("message") ? request.body.message : " ";
        let attachment = request.body.hasOwnProperty("attachment") ? request.body.attachment : null;

        if (attachment === null) {
            logger.text.error("You must specify an attachment. No attachment found.");
            response.status(400);
            response.json({
                "success": false,
                "error": "You must specify an attachment. No attachment found."
            })
        }

        let attachment_name = attachment.hasOwnProperty("filename") ? attachment.filename : null;
        let attachment_type = attachment.hasOwnProperty("contentType") ? attachment.contentType : null;
        let attachment_data = attachment.hasOwnProperty("data") ? attachment.data : null;

        if (attachment_name === null || attachment_type === null || attachment_data === null) {
            logger.text.error("Missing attachment data, you must specify fields name, contentType and data");
            response.status(400);
            return response.json({
                "success": false,
                "error": "Missing attachment data, you must specify fields name, contentType and data"
            });
        }

        let fileBuffer = Buffer.from(attachment_data, 'base64');
        let fileHash = crypto.createHash('sha1').update(attachment_data).digest('hex');
        let tempPath = path.join(config.attachment.temp, attachment_name + "." + fileHash);

        if (request.body.hasOwnProperty("userLogin") ) {
            let loginEmail = request.body.userLogin;

            fs.writeFile(tempPath, fileBuffer, (err) => {
                if(err) {
                    logger.text.error("Got an error while creating file for userLogin: ", err);
                    response.json({
                        "success": false,
                        "error": error
                    });
                }
                else {
                    logger.text.info("File created successfully");
                    sendAttachment(loginEmail, {
                        "filename": attachment_name,
                        "fileSize": fileBuffer.byteLength,
                        "path": tempPath,
                        "contentType": attachment_type,
                        "message": message
                    }).then(() => {
                        logger.text.info("File sent successfully to userLogin");
                        response.json({
                            "success": true
                        });
                    }).catch((error) => {
                        logger.text.error("Error sending file to userLogin: ", error);
                        response.status(500);
                        response.json({
                            "success": false,
                            "error": error
                        });
                    });
                }
            });
        } else if (request.body.hasOwnProperty("bubble") ) {
            let jid = request.body.bubble;

            fs.writeFile(tempPath, fileBuffer, (err) => {
                if(err) {
                    logger.text.error("Got an error while creating file for bubble : ", err);
                    response.json({
                        "success": false,
                        "error": error
                    });
                }
                else {
                    sendAttachmentBubble(jid, {
                        "filename": attachment_name,
                        "fileSize": fileBuffer.byteLength,
                        "path": tempPath,
                        "contentType": attachment_type,
                        "message": message
                    }).then(() => {
                        logger.text.info("File sent successfully to bubble");
                        response.json({
                            "success": true
                        });
                    }).catch((error) => {
                        logger.text.error("Error sending file to bubble: ", error);
                        response.status(500);
                        response.json({
                            "success": false,
                            "error": error
                        });
                    });
                }
            });
        } else {
            logger.text.error("Missing user email or jid");
            response.status(400);
            response.json({
                "success": false,
                "error": "Error: You must provide either a user email or a jid"
            });
        }
    });

    app.get("/rainbow-gateway/check-rainbow-connection", function (request, response) {
        response.status(200).json({"status":rainbow.state});
    });

    app.listen(PORT, (err) => {
        if (err) {
            logger.text.error("Error: " + err);
        } else {
            logger.text.debug("Server is listening on port " + PORT);
        }
    });

});
