const winston = require('winston');
require('winston-daily-rotate-file');
const util = require("util");
const stripAnsi = require('strip-ansi');
const os = require('os');
const cson = require("cson");
const config = cson.load(process.env.RAINBOW_GATEWAY_CONFIG_PATH || "config.cson");

const tsFormat = () => {
  let date = new Date();
  return date.toLocaleDateString() + " " + date.toLocaleTimeString() + " [" + date.valueOf() + "]";
};

const myFormat = winston.format.printf(info => {
  return `${tsFormat()}` + ' - ' + stripAnsi(info.level) + ':' + stripAnsi(info.message);
});

const argumentsToString = (v)=>{
  var args = Array.prototype.slice.call(v, 0);
  for(var k in args){
      if (typeof args[k] === "object"){
          args[k] = util.inspect(args[k], false, null, true);
      }
  }
  var str = args.join(" ");
  return str;
}
winston.loggers.add('logger_text', {
  level: config.logs.file.level,
  format: winston.format.combine(
    winston.format.splat(),
    myFormat
  ),
  transports: [
    new (winston.transports.DailyRotateFile)({
      dirname: config.logs.file.path,
      filename: 'rainbow-gateway.log.%DATE%',
      datePattern: 'YYYYMMDD',
      level : config.logs.file.level,
      timestamp: tsFormat,
      maxFiles: config.logs.retention,
      prepend: true,
    })
  ]
});

const MESSAGE = Symbol.for('message');
const jsonFormatter = (logEntry) => {
  let date = new Date();
  const base = { "timestamp": date.toISOString(),"hostname": os.hostname()};
  const json = Object.assign(base,logEntry["message"])
  logEntry[MESSAGE] = JSON.stringify(json);
  return logEntry;
  }

winston.loggers.add('logger_json', {
  level: config.logs.file.level,
  format: winston.format(jsonFormatter)(),
  transports: [
      new (winston.transports.DailyRotateFile)({
          dirname: config.logs.file.path,
          filename: 'rainbow-gateway-journal.log.%DATE%',
          datePattern: 'YYYYMMDD',
          level : config.logs.file.level,
          timestamp: new Date().toISOString(),
          maxFiles: config.logs.retention,
          prepend: true,
        })
  ]
});

const logger_text = winston.loggers.get('logger_text');
const logger_json = winston.loggers.get('logger_json');


const wrap ={};
wrap.info = function () {logger_text.log.apply(logger_text, ["info", argumentsToString(arguments)]);};
wrap.error = function () {logger_text.log.apply(logger_text, ["error", argumentsToString(arguments)]);};
wrap.warn = function () { logger_text.log.apply(logger_text, ["warn", argumentsToString(arguments)]);};
wrap.debug = function () {logger_text.log.apply(logger_text, ["debug", argumentsToString(arguments)]);};
wrap.log = function (level) {logger_text.log.apply(logger_text, [level, argumentsToString(arguments)]);};

const wrap_json ={};
wrap_json.debug =  function () {logger_json.debug(arguments['0']);}
wrap_json.info =  function () {logger_json.info(arguments['0']);}
wrap_json.warn =  function () {logger_json.warn(arguments['0']);}
wrap_json.error =  function () {logger_json.error(arguments['0']);}


module.exports = {
    text: wrap,
    json: wrap_json
};

